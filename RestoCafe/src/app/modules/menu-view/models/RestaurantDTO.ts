import { TableMenuDTO } from "./TableMenuDTO";

export interface RestaurantDTO {
  restaurant_id: null,
  restaurant_name: null,
  restaurant_image: null,
  table_id: null,
  table_name: null,
  branch_name: null,
  nexturl: null,
  table_menu_list: TableMenuDTO[]
}