import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from './modules/common/services/auth-guard.service';
import { PageNotFoundComponent } from './modules/utils/page-not-found/page-not-found.component';

const routes: Routes = [
  { path: '', redirectTo: '/auth/home', pathMatch: 'full' },
  { path: 'auth', loadChildren: () => import('./modules/auth/auth.module').then(m => m.AuthModule) },
  { path: 'orders', loadChildren: () => import('./modules/menu-view/menu-view.module').then(m => m.MenuViewModule) },
  { path: 'common', loadChildren: () => import('./modules/common/common.module').then(m => m.CommonModule) },
  { path: '**', component: PageNotFoundComponent, canActivate: [AuthGuardService] },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
