export interface CategoryDishesDTO {
  dish_id: null,
  dish_name: null,
  dish_price: null,
  dish_image: null,
  dish_currency: null,
  dish_calories: null,
  dish_description: null,
  dish_Availability: null,
  dish_Type: null,
  nexturl: null,
  addonCat: null
}