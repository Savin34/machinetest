import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { RestaurantDTO } from '../../models/RestaurantDTO';
import { TableMenuDTO } from '../../models/TableMenuDTO';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.scss']
})
export class OrdersComponent implements OnInit {

  loading: boolean = true;
  restaurants: RestaurantDTO[] = [];
  tableMenus: TableMenuDTO[] = [];
  isRestoImageAvailable = true;
  menuDetails: any;
  dishes: any;
  card: any = {};
  totalOrderCount = 0;
  constructor(
    private http: HttpClient
  ) { }

  ngOnInit() {
    this.getAllMenus();
  }
  getAllMenus() {
    this.http.get('https://www.mocky.io/v2/5dfccffc310000efc8d2c1ad').subscribe((restaurants: any) => {
      if (restaurants) {
        this.restaurants = restaurants;
        this.loading = false;
        this.menuDetails = this.getAllCategories(this.restaurants[0]);
        console.log(this.restaurants[0], 'restorationlllll');
      }
    })
  }
  setDefaultPic() {
    this.isRestoImageAvailable = false;
  }
  getAllCategories(restaurants: any) {
    let categories: any[] = [];
    let dishes: any[] = [];
    let orders: any[] = [];
    restaurants.table_menu_list.map((menu:any) => {
      categories.push({'id':menu.menu_category_id, 'name': menu.menu_category});
      menu.category_dishes.map((dish:any) => {
        dishes.push({'id':dish.dish_id ,'name': dish.dish_name, 'count':0 })
      });
    })
    this.totalOrderCount = dishes.reduce((pre, curr) => pre + curr.count, 0);
    return { categories, dishes, orders };
  }
  getDishesByCategory(category:any) {
    let dishes = this.restaurants[0].table_menu_list.find(x => x.menu_category_id === category.id);
    if (dishes && dishes.category_dishes) {
      this.dishes = dishes.category_dishes;
      
      this.dishes.forEach((dish:any, i:number)=> {
        dish.count = 0;
        dish.dish_price_temp = JSON.parse(JSON.stringify(dish.dish_price));
      });
    }
  }
  getDishesByCategoryTab(category:any) {
    let dishes = this.restaurants[0].table_menu_list.find(x => x.menu_category === category.tab.textLabel);
    if (dishes && dishes.category_dishes) {
      this.dishes = dishes.category_dishes;
      this.dishes.forEach((dish:any)=> {
        dish.count = 0;
        dish.dish_price_temp = JSON.parse(JSON.stringify(dish.dish_price));
      });
    }
  }
  showNoImageAvailable() {
    return true;
  }
  decrement(index: number) {
    if (this.dishes[index].count > 0) {
      this.dishes[index].disableDecrement = false;
      this.dishes[index].count = this.dishes[index].count-1;
      this.totalOrderCount = this.totalOrderCount - 1;
      this.dishes[index].dish_price = this.dishes[index].dish_price_temp * this.dishes[index].count;
    } else {
      this.dishes[index].disableDecrement = true;
    }
    
  }
  increment(index: number) {
    this.dishes[index].disableDecrement = false;
    this.dishes[index].count = this.dishes[index].count+1;
    this.totalOrderCount = this.totalOrderCount + 1;
    this.dishes[index].dish_price = this.dishes[index].dish_price_temp * this.dishes[index].count;
  }
}
