import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MenuViewRoutingModule } from './menu-view-routing.module';
import { OrdersComponent } from './components/orders/orders.component';
import { SharedModule } from '../shared/shared.module';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [OrdersComponent],
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    HttpClientModule,
    MenuViewRoutingModule
  ]
})
export class MenuViewModule { }
