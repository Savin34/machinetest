import { NgModule } from '@angular/core';

import { CommonRoutingModule } from './common-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    CommonRoutingModule
  ]
})
export class CommonModule { }
