import { CategoryDishesDTO } from "./CategoryDishesDTO";

export interface TableMenuDTO {
  menu_category: null,
  menu_category_id: null,
  menu_category_image: null,
  nexturl: null,
  category_dishes: CategoryDishesDTO[]
}